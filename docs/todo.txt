=in progress=
diplomatic state
	declare war
	offer peace
	accept peace
	revoke peace offer

=next steps=
CLI params to automatically connect and make new game

for server make per-socket instance of the waiting data field (in the dict)

fighting, health (in hundredth)
end turn, health regeneration

move units: movement cost: if any movement points (in hundreds) left, you can move. minus gets carried over to next turn.


reconnect

command to request debug print from server
found cities
respect unit ability to enter sea/land
work terrain
write & load items.ini
build units
write & load buildings.ini
build buildings
save&load
write & load researches.ini
researching
make combat effectiveness depend on relative height
culture/tile ownership
randomness (but with seed to make it reproducible)
decent map generator (look at freecol's)

map editor mode, map save/load

=essential security/cleanup=
create standard files like COPYING and contributors and stuff
dont crash when trying to move a unit but none is selected
transaction system (like in SQL) for map updates, to avoid redrawing the map a billion times
maybe rename network command to network message
check docs whether on_ready_read can be called whilst it is currently running (ie whether this is guaranteed not to happen -> if it isn't, do something)
protocol version in connect
close the sockets
SSL
passwords

=cleanup=
add docstrings, reactivate pylint test for that
mypy, pytype
unittests

=docs=
code of conduct
contributor agreement
how to submit security bugs
make howto for adding new c2s and new s2c network message

=comfort=
disconnect feature
deactivate unavailable menu functions
make server sends client a done_sending informational command (to cause redraw)

=non-essential features=
switch to 3D map?
