#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib
import signal
import sys

#pyqt
from PyQt5.QtWidgets import QApplication
from PyQt5.QtNetwork import QHostAddress, QTcpServer

#other libs
import msgpack

#own modules
from game import Game
from wiab_map import WiabMap
from wiab_mod import WiabMod

class WiabServer(QApplication):
	def __init__(self, args):
		super(WiabServer, self).__init__(args)
		
		self.wiab_mod = WiabMod(False)
		
		self.emitters = {b"connect_c2s":self.received_connect, b"move_c2s":self.received_move, b"start_new_game_c2s":self.received_start_new_game}
		
		self.data_to_process = b''
		
		self.client_socket_dicts = {} #player_id:{"socket":QTcpSocket, "id":int, "player_name":str}
		self.tcp_server = QTcpServer()
		self.tcp_server.newConnection.connect(self.new_connection)
		self.tcp_server.listen(QHostAddress("127.0.0.1"), 49000)
		
		#TODO what is this timer for
		#self.incomplete_message_timer = QTimer()
		#self.incomplete_message_timer.timeout.connect(self.on_ready_read)
		
		self.game = None
	
	def new_connection(self):
		while self.tcp_server.hasPendingConnections():
			new_client_socket = self.tcp_server.nextPendingConnection()
			#TODO new_client_socket.disconnected.connect(self.client_socket_disconnected)
			new_client_socket.readyRead.connect(self.on_ready_read)
			print("connected", new_client_socket.peerAddress())
	
	def on_ready_read(self, sender=None, new_data=None, recursively_called=False):
		if not recursively_called:
			self.data_to_process += self.sender().readAll().data()
			sender = self.sender()
		
		#datastring = ""
		#for i in self.data_to_process:
		#	datastring += "%d," % i
		#print("data_to_process", datastring)
		
		if len(self.data_to_process) < 4:
			return #didnt receive length field yet
		
		length = 256*256*256*self.data_to_process[0] + 256*256*self.data_to_process[1] + 256*self.data_to_process[2] + self.data_to_process[3] #TODO use stdlib here
		
		if len(self.data_to_process) < length:
			return #didnt receive full message yet
		
		message = msgpack.unpackb(self.data_to_process[4:4+length])
		print(message)
		
		self.emitters[message[0]](sender, message)
		
		self.data_to_process = self.data_to_process[4+length:]
		if len(self.data_to_process) > 0:
			self.on_ready_read(sender=sender, new_data=None, recursively_called=True)
	
	def received_connect(self, sender, message):
		player_name = message[1].decode("UTF8")
		for socket_dict in self.client_socket_dicts.values():
			if socket_dict["player_name"] == player_name: #reconnect
				self.client_socket_dicts[socket_dict["player_id"]]["socket"] = sender
				self.send_player_info_to_all()
				print("%s reconnected" % player_name)
				return
		
		if self.game:
			print("cannot connect new player to running game") #TODO send error to client
			return
		
		if len(self.client_socket_dicts.items()) > 0:
			player_id = max(self.client_socket_dicts.keys()) + 1
		else:
			player_id = 1 #start at since id 0 is used for unknown
		self.client_socket_dicts[player_id] = {"socket":sender, "player_name":player_name, "player_id":player_id}
		self.send_player_info_to_all()
		print("recorded socket for new player %s with id %d" % (player_name, player_id))
	
	def received_move(self, sender, message):
		#TODO check ownership
		result = self.game.wiab_map.move(message[1], message[2], message[3], self.socket_to_player_id(sender))
		if result == WiabMap.success:
			self.send_current_visible_to_all()
	
	def received_start_new_game(self, sender, message):
		size_x = message[1]
		size_y = message[2]
		
		self.game = Game(False, self.wiab_mod, size_x, size_y, client_socket_dicts=self.client_socket_dicts)
		self.send_all("start_new_game_s2c", size_x, size_y)
		self.send_current_visible_to_all()
	
	def send_all(self, *args):
		message = msgpack.packb(args)
		length = len(message).to_bytes(4, byteorder="big")
		for socket_dict in self.client_socket_dicts.values():
			socket_dict["socket"].write(length + message)
			socket_dict["socket"].flush()
	
	def send_current_visible_to_all(self):
		for player_id in self.client_socket_dicts.keys():
			visible_tiles = self.game.get_visible_tiles(player_id)
			for tile in visible_tiles:
				self.send_one(player_id, "tile_info_s2c", tile.owner.player_id, tile.location[0], tile.location[1], tile.terrain)
				if tile.unit:
					self.send_one(player_id, "unit_info_s2c", tile.unit.owner.player_id, tile.location[0], tile.location[1], tile.unit.thing_id, tile.unit.type_name)
	
	def send_player_info_to_all(self):
		for socket_dict in self.client_socket_dicts.values():
			self.send_all("new_player_s2c", socket_dict["player_id"], socket_dict["player_name"])
	
	def send_one(self, player_id, *args):
		message = msgpack.packb(args)
		length = len(message).to_bytes(4, byteorder="big")
		self.client_socket_dicts[player_id]["socket"].write(length + message)
		self.client_socket_dicts[player_id]["socket"].flush()
	
	def socket_to_player_id(self, socket):
		for socket_dict in self.client_socket_dicts.values():
			if socket_dict["socket"] == socket:
				return socket_dict["player_id"]

if __name__ == '__main__':
	signal.signal(signal.SIGINT, signal.SIG_DFL) #makes ctrl+C at the shell work
	app = WiabServer(sys.argv)
	
	sys.exit(app.exec_())
