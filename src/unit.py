# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib

#pyqt

#other libs

#own modules
from has_health import HasHealth

class Unit(HasHealth):
	def __init__(self, owner, location, thing_id, type_name):
		super(Unit, self).__init__(owner, location, thing_id)
		self.moved_this_turn = False #allows newly created units to move immediately
		self.type_name = type_name #TODO move up into Thing
	
	def end_turn(self):
		super(Unit, self).end_turn()
		self.moved_this_turn = False
	
	def status_bar_string(self):
		if self.moved_this_turn:
			moved_string = "has moved this turn"
		else:
			moved_string = "has not moved this turn"
		return "%s at (%d,%d), %s" % (self.type_name, self.location[0], self.location[1], moved_string)
