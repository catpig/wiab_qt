# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib
from random import randint

#pyqt

#other libs

#own modules
from tile import Tile
from unit import Unit

class WiabMap(object):
	default_tile_terrain = "plains" #TODO move into mod
	success = 0
	target_occupied = 1
	different_player_id = 2
	
	def __init__(self, is_client, size_x, size_y, unowned_player):
		self.size_x = size_x
		self.size_y = size_y
		self.tiles = []
		self.units = {} #id:Unit
		self.is_client = is_client
		
		for pos_x in range(size_x):
			inner = []
			for pos_y in range(size_y):
				if self.is_client:
					inner.append(Tile(unowned_player, (pos_x, pos_y), None, "unknown"))
				else:
					inner.append(Tile(unowned_player, (pos_x, pos_y), None, WiabMap.default_tile_terrain))
			self.tiles.append(inner)
		
		#print(self.tiles)
	
	def get_location_random(self):
		return (randint(0, self.size_x-1), randint(0, self.size_y-1))
	
	def get_location_offset(self, location_orig, location_offset): #TODO rename to add_locations or something
		pos_x = location_orig[0]+location_offset[0]
		pos_y = location_orig[1]+location_offset[1]
		
		while pos_x < 0:
			pos_x += self.size_x
		while pos_x >= self.size_x:
			pos_x -= self.size_x
		
		while pos_y < 0:
			pos_y += self.size_y
		while pos_y >= self.size_y:
			pos_y -= self.size_y
		
		return (pos_x, pos_y)
	
	def get_locations_radius(self, location): #TODO allow setting radius
		return (
			self.get_location_offset(location, (-1, -1)),
			self.get_location_offset(location, (0, -1)),
			self.get_location_offset(location, (1, -1)),
			self.get_location_offset(location, (-1, 0)),
			location,
			self.get_location_offset(location, (1, 0)),
			self.get_location_offset(location, (-1, 1)),
			self.get_location_offset(location, (0, 1)),
			self.get_location_offset(location, (1, 1))
		)
	
	def move(self, target_x, target_y, thing_id, requesting_player_id): #TODO verify terrain, movement points
		if self.tiles[target_x][target_y].unit:
			print("TODO s2c error: target field for move already occupied")
			return WiabMap.target_occupied
		
		if self.units[thing_id].owner.id != requesting_player_id:
			print("TODO s2c error: cannot move other player's units. unit id %d, requestor id %d" % (self.units[thing_id].owner.id, requesting_player_id))
			return WiabMap.different_player_id
		
		self.tiles[self.units[thing_id].location[0]][self.units[thing_id].location[1]].unit = None
		self.tiles[target_x][target_y].unit = self.units[thing_id]
		self.units[thing_id].location = (target_x, target_y)
		return WiabMap.success
	
	def place_new_unit(self, owner, location, unit_name, nearby_if_impossible):
		if self.tiles[location[0]][location[1]].unit: #already a unit present
			if nearby_if_impossible:
				self.place_new_unit(owner, self.get_location_offset(location, (1, 0)), unit_name, nearby_if_impossible)
			else:
				print("TODO raiseTileOccupiedException")
		else:
			new_unit = Unit(owner, location, None, unit_name)
			self.units[new_unit.thing_id] = new_unit
			owner.units.append(new_unit)
			self.tiles[location[0]][location[1]].unit = new_unit
