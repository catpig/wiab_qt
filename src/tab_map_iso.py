#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib

#pyqt
from PyQt5.QtCore import pyqtSignal, QLineF, QObject, QPoint, QPointF
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView

#other libs

#own modules
ISO_IMAGE_FILE_HEIGHT = 64
ISO_TILE_HEIGHT = 32
ISO_TILE_WIDTH = 64
ISO_Z_TERRAIN = 0.0
ISO_Z_IMPROVEMENT_NON_TRANSPORT = 1.0
ISO_Z_IMPROVEMENT_TRANSPORT = 2.0
ISO_Z_CITY = 3.0
ISO_Z_UNIT = 4.0
ISO_Z_GRID = 5.0

class TabMapIso(QObject):
	map_clicked = pyqtSignal(int, int, int)
	
	def __init__(self, parent, wiab_mod): #TODO find what parent did exactly and then apply it accordingly everywhere
		super(TabMapIso, self).__init__()
		self.active_player_id = 0
		self.game = None
		self.wiab_mod = wiab_mod
		self.parent = parent
		self.map_size_x = -1
		self.map_size_y = -1
		
		self.scene = QGraphicsScene()
		self.scroller = QGraphicsView(self.scene)
		
		self.scene.mouseReleaseEvent = self.mouseReleaseEvent
	
	def convert_screen_to_tile(self, pixel_x, pixel_y):
		tile_x = ((pixel_y - self.scroller.height()/2)*2/ISO_TILE_HEIGHT + 3 + 2*pixel_x/ISO_TILE_WIDTH)/2
		tile_y = 2*pixel_x/ISO_TILE_WIDTH - (((pixel_y - self.scroller.height()/2)*2/ISO_TILE_HEIGHT + 3 + 2*pixel_x/ISO_TILE_WIDTH)/2)
		result_x = tile_x + 30
		result_y = tile_y - 30
		if result_x < 0:
			result_x = -1
		if result_y < 0:
			result_y = -1
		return (int(result_x), int(result_y))
	
	def convert_tile_to_screen(self, tile_x, tile_y):
		pixel_x = ISO_TILE_WIDTH/2*(tile_x+tile_y)
		pixel_y = self.scroller.height()/2 + (tile_x-tile_y-3)/2*ISO_TILE_HEIGHT #the -3 is to correct for the pictures starting 48px higher
		return (pixel_x, pixel_y)
	
	def draw(self):
		self.active_player_id = self.parent.active_player_id
		self.game = self.parent.game
		self.map_size_x = self.parent.size_x
		self.map_size_y = self.parent.size_y
		
		for item in self.scene.items():
			self.scene.removeItem(item) #TODO i think i might need to destroy the return of this explicitly
		
		self.draw_map()
		#self.draw_grid() #TODO this causes the pixmaps to no longer be drawn
	
	def draw_grid(self):
		height = self.scroller.height()
		
		for tile_y in range(0, self.map_size_y+1):
			start = QPointF(ISO_TILE_WIDTH/2*tile_y, height/2-ISO_TILE_HEIGHT/2*tile_y)
			end = QPointF(ISO_TILE_WIDTH/2*(tile_y+self.map_size_x), height/2+ISO_TILE_HEIGHT/2*(self.map_size_x-tile_y))
			line = self.scene.addLine(QLineF(start, end))
			line.setZValue(ISO_Z_GRID)
		
		for tile_x in range(0, self.map_size_x+1):
			start = QPointF(ISO_TILE_WIDTH/2*tile_x, height/2+ISO_TILE_HEIGHT/2*tile_x)
			end = QPointF(ISO_TILE_WIDTH/2*(tile_x+self.map_size_y), height/2+ISO_TILE_HEIGHT/2*(tile_x-self.map_size_y))
			line = self.scene.addLine(QLineF(start, end))
			line.setZValue(ISO_Z_GRID)
	
	def draw_map(self):
		print("start of draw_map self.scroller.height:", self.scroller.height())
		
		#draw basetypes
		for tile_x in range(0, self.map_size_x):
			for tile_y in range(0, self.map_size_y):
				pixel_x, pixel_y = self.convert_tile_to_screen(tile_x, tile_y)
				pixel_y = pixel_y-ISO_TILE_HEIGHT/2*(ISO_IMAGE_FILE_HEIGHT-1)
				
				pixmap_item = self.scene.addPixmap(self.wiab_mod.terrain_pixmaps[self.game.wiab_map.tiles[tile_x][tile_y].terrain])
				pixmap_item.setOffset(QPoint(pixel_x, pixel_y))
				pixmap_item.setZValue(ISO_Z_TERRAIN)
				
				if self.game.wiab_map.tiles[tile_x][tile_y].unit:
					print("about to try drawing unit for %d,%d" % (tile_x, tile_y))
					pixmap_unit = self.scene.addPixmap(self.wiab_mod.unit_pixmaps[self.game.wiab_map.tiles[tile_x][tile_y].unit.type_name])
					pixmap_unit.setOffset(QPoint(pixel_x, pixel_y))
					pixmap_unit.setZValue(ISO_Z_UNIT)
	
	def mouseReleaseEvent(self, event): #TODO dont emit left/rightClicked but call methods
		pixel_x = event.scenePos().x()
		pixel_y = event.scenePos().y()
		tile_x, tile_y = self.convert_screen_to_tile(pixel_x, pixel_y)
		print("mouseReleaseEvent: corrected screen pos", pixel_x, pixel_y, "tile (%d,%d)" % (tile_x, tile_y))
		
		if (tile_x >= 0 and tile_y >= 0 and tile_x < self.map_size_x and tile_y < self.map_size_y):
			self.map_clicked.emit(tile_x, tile_y, event.button())
