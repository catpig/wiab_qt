# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib

#pyqt

#other libs

#own modules
from player import Player
from wiab_map import WiabMap

class Game(object):
	def __init__(self, is_client, wiab_mod, size_x, size_y, client_socket_dicts=None, players_dict=None):
		self.current_tick = 0
		self.is_client = is_client
		
		self.players = {} #player_id:Player
		self.players[0] = Player(0, "Unowned")
		if not is_client:
			for socket_dict in client_socket_dicts.values():
				self.players[socket_dict["player_id"]] = Player(socket_dict["player_id"], socket_dict["player_name"])
		else:
			for player_id, name in players_dict.items():
				self.players[player_id] = Player(player_id, name)
		
		self.set_initial_diplomatic_states()
		
		self.wiab_mod = wiab_mod
		self.wiab_map = WiabMap(is_client, size_x, size_y, self.players[0])
		
		self.place_starting_units()
	
	def get_visible_tiles(self, player_id):
		locations = []
		for unit in self.players[player_id].units:
			for location in self.wiab_map.get_locations_radius(unit.location):
				if location not in locations:
					locations.append(location)
		
		result = []
		for location in locations:
			result.append(self.wiab_map.tiles[location[0]][location[1]])
		
		return result
	
	def place_starting_units(self):
		for player in self.players.values():
			location = self.wiab_map.get_location_random()
			for unit_name in self.wiab_mod.starting_units:
				self.wiab_map.place_new_unit(player, location, unit_name, True)
	
	def set_initial_diplomatic_states(self):
		for player_a in self.players.values():
			for player_b in self.players.values():
				if player_a != player_b:
					player_a.diplomatic_states[player_b.player_id] = "neutral" #only need to do it in one direction since we get to the reverse later in the loop
