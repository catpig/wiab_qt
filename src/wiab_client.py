#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib
import signal
import sys

#pyqt
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtNetwork import QTcpSocket
from PyQt5.QtWidgets import QApplication, QMainWindow, QMenu, QMenuBar, QStatusBar, QTabWidget

#other libs
import msgpack

#own modules
from dia_connect import DiaConnect
from dia_new_game import DiaNewGame
from game import Game
from tab_map_iso import TabMapIso
from tab_players import TabPlayers
from unit import Unit
from wiab_mod import WiabMod

class WiabClient(QMainWindow):
	def __init__(self):
		super(WiabClient, self).__init__()
		
		self.game = None
		self.active_player_name = None
		self.active_player_id = 0
		self.players = {} #player_id:player_name
		self.active_unit_thing_id = None
		self.wiab_mod = WiabMod(True)
		
		self.emitters = {b"new_player_s2c":self.received_new_player, b"start_new_game_s2c":self.received_start_new_game, b"tile_info_s2c":self.received_tile_info, b"unit_info_s2c":self.received_unit_info}
		
		self.socket = QTcpSocket()
		self.data_to_process = b''
		
		self.tab_widget = QTabWidget()
		self.setCentralWidget(self.tab_widget)
		
		tab = TabMapIso(self, self.wiab_mod)
		tab.map_clicked.connect(self.map_clicked)
		self.tab_widget.addTab(tab.scroller, "Map")
		self.tabs = {"map":tab}
		
		tab = TabPlayers(self)
		self.tab_widget.addTab(tab.scroller, "Players")
		self.tabs["players"] = tab
		
		self.tab_widget.setCurrentIndex(0)
		
		self.status_bar = QStatusBar(self)
		self.setStatusBar(self.status_bar)
		
		self.setup_menu_bar()
		#self.show()
		self.showMaximized()
	
	def closeEvent(self, event=None):
		print("quitting normally")
		sys.exit(0)
	
	def connect_dia(self):
		print("connect_dia called")
		dia = DiaConnect(self)
		dia.connect_clicked.connect(self.connect_dia_clicked)
		dia.exec()
	
	def connect_dia_clicked(self, host, port, player_name):
		print("connect_dia_connect_clicked", host, port, player_name)
		self.socket.close()
		self.active_player_name = player_name
		self.socket.connectToHost(host, port)
		self.socket.connected.connect(self.connected)
	
	def connected(self):
		self.socket.readyRead.connect(self.on_ready_read)
		
		self.send("connect_c2s", self.active_player_name)
		self.status_bar.showMessage("connected to %s:%d" % (self.socket.peerName(), self.socket.peerPort())) 
	
	def map_clicked(self, tile_x, tile_y, button_code):
		print("client.map_clicked", tile_x, tile_y, button_code)
		
		if button_code == Qt.LeftButton:
			if self.game.wiab_map.tiles[tile_x][tile_y].unit:
				self.active_unit_thing_id = self.game.wiab_map.tiles[tile_x][tile_y].unit.thing_id
				self.refresh_status_bar()
		else:
			print("unrecognised button code:", button_code)
		#== Qt.RightButton):
	
	def move(self, move_x, move_y):
		target_location = self.game.wiab_map.get_location_offset(self.game.wiab_map.units[self.active_unit_thing_id].location, (move_x, move_y))
		self.send("move_c2s", target_location[0], target_location[1], self.active_unit_thing_id)
	
	def move_e(self):
		self.move(1, 1)
	
	def move_n(self):
		self.move(-1, 1)
	
	def move_ne(self):
		self.move(0, 1)
	
	def move_nw(self):
		self.move(-1, 0)
	
	def move_s(self):
		self.move(1, -1)
	
	def move_se(self):
		self.move(1, 0)
	
	def move_sw(self):
		self.move(0, -1)
	
	def move_w(self):
		self.move(-1, -1)
	
	def new_game_dia(self):
		#if self.socket.state == QAbstractSocket.ConnectedState: #TODO why doesnt this work?
			dia = DiaNewGame(self)
			dia.new_game_clicked.connect(self.new_game_dia_clicked)
			dia.exec()
		#else:
		#	self.status_bar.showMessage("not connected to a server")
	
	def new_game_dia_clicked(self, size_x, size_y):
		self.send("start_new_game_c2s", size_x, size_y)
	
	def on_ready_read(self, sender = None, new_data = None, recursively_called = False):
		if not recursively_called:
			self.data_to_process += self.sender().readAll().data()
			sender = self.sender()
		
		#datastring = ""
		#for i in self.data_to_process:
			#datastring += "%d," % i
		#print("data_to_process", datastring)
		
		if len(self.data_to_process) < 4:
			return #didnt receive length field yet
		
		length = 256*256*256*self.data_to_process[0] + 256*256*self.data_to_process[1] + 256*self.data_to_process[2] + self.data_to_process[3] #TODO use stdlib here
		
		if len(self.data_to_process) < length:
			return #didnt receive full message yet
		
		message = msgpack.unpackb(self.data_to_process[4:4+length])
		print(message)
		
		self.emitters[message[0]](sender, message)
		
		self.data_to_process = self.data_to_process[4+length:]
		if len(self.data_to_process) > 0:
			self.on_ready_read(sender=sender, new_data=None, recursively_called = True)
	
	def print_tiles(self):
		print(self.game.wiab_map.tiles)
	
	def received_tile_info(self, sender, message):
		location_x = message[2]
		location_y = message[3]
		
		self.game.wiab_map.tiles[location_x][location_y].owner = self.game.players[message[1]]
		self.game.wiab_map.tiles[location_x][location_y].terrain = message[4].decode("UTF8")
		self.game.wiab_map.tiles[location_x][location_y].unit = None
		self.tabs["map"].draw()
		print("received_full_tile_info", message[1], location_x, location_y, message[4].decode("UTF8"))
	
	def received_unit_info(self, sender, message):
		location_x = message[2]
		location_y = message[3]
		
		new_unit = Unit(self.game.players[message[1]], (location_x, location_y), message[4], message[5].decode("UTF8"))
		self.game.wiab_map.tiles[location_x][location_y].unit = new_unit
		self.game.wiab_map.units[message[4]] = new_unit
		print("received_full_unit_info", message[1], location_x, location_y, message[4], message[5].decode("UTF8"))
	
	def received_new_player(self, sender, message):
		new_player = message[2].decode("UTF8")
		if new_player == self.active_player_name:
			self.active_player_id = message[1]
		
		self.players[message[1]] = new_player
		self.status_bar.showMessage("connected as %s with ID %d" % (self.active_player_name, self.active_player_id)) 
	
	def received_start_new_game(self, sender, message):
		self.size_x = message[1]
		self.size_y = message[2]
		self.game = Game(True, self.wiab_mod, message[1], message[2], players_dict = self.players)
		self.tabs["map"].draw()
		self.tabs["players"].draw()
	
	def redraw_map(self): #TODO can i give this directly in the menu?
		self.tabs["map"].draw()
	
	def refresh_status_bar(self):
		message = ""
		if self.active_unit_thing_id:
			message += self.game.wiab_map.units[self.active_unit_thing_id].status_bar_string()
		
		self.status_bar.showMessage(message)
	
	def send(self, *args):
		message = msgpack.packb(args)
		length = len(message).to_bytes(4, byteorder="big")
		self.socket.write(length + message)
		self.socket.flush()
	
	def setup_menu_bar(self):
		self.menu_bar = QMenuBar(self)
		self.setMenuBar(self.menu_bar)
		
		self.menu_game = QMenu("Game", self.menu_bar)
		self.menu_game.addAction("Connect", self.connect_dia, QKeySequence("Ctrl+C"))
		self.menu_game.addAction("New Game", self.new_game_dia, QKeySequence("Ctrl+N"))
		self.menu_game.addSeparator()
		self.menu_game.addAction(QIcon.fromTheme("application-exit"), "Quit", self.closeEvent, QKeySequence("Ctrl+Q"))
		self.menu_bar.addAction(self.menu_game.menuAction())
		
		self.menu_unit = QMenu("Unit", self.menu_bar)
		self.menu_unit.addAction("Move Northwest", self.move_nw, QKeySequence("7"))
		self.menu_unit.addAction("Move North", self.move_n, QKeySequence("8"))
		self.menu_unit.addAction("Move Northeast", self.move_ne, QKeySequence("9"))
		self.menu_unit.addAction("Move West", self.move_w, QKeySequence("4"))
		self.menu_unit.addAction("Move East", self.move_e, QKeySequence("6"))
		self.menu_unit.addAction("Move Southwest", self.move_sw, QKeySequence("1"))
		self.menu_unit.addAction("Move South", self.move_s, QKeySequence("2"))
		self.menu_unit.addAction("Move Southeast", self.move_se, QKeySequence("3"))
		self.menu_bar.addAction(self.menu_unit.menuAction())
		
		self.menu_debug = QMenu("Debug", self.menu_bar)
		self.menu_debug.addAction("Print Tiles", self.print_tiles)
		self.menu_debug.addAction("Redraw Map", self.redraw_map)
		self.menu_bar.addAction(self.menu_debug.menuAction())

if __name__ == '__main__':
	app = QApplication(sys.argv)
	signal.signal(signal.SIGINT, signal.SIG_DFL) #makes ctrl+C at the shell work
	wiab = WiabClient()
	
	sys.exit(app.exec_())
