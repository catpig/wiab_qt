#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib
from configparser import ConfigParser
import os

#pyqt

#other libs

#own modules

class WiabMod(object):
	buildings = {}
	items = {}
	starting_units = []
	terrain_pixmaps = {}
	terrains = {}
	unit_pixmaps = {}
	units = {}
	
	def __init__(self, is_client):
		if is_client:
			from PyQt5.QtGui import QPixmap
			WiabMod.terrain_pixmaps["unknown"] = QPixmap(os.path.join('mods', 'default', 'terrains', 'unknown.png'))
		
		config = ConfigParser()
		config.read(os.path.join('mods', 'default', 'buildings.ini'))
		for section in config.sections():
			WiabMod.buildings[section] = {}
			for key in config[section]:
				WiabMod.buildings[section][key] = config[section][key]
		
		config = ConfigParser()
		config.read(os.path.join('mods', 'default', 'items.ini'))
		for section in config.sections():
			WiabMod.items[section] = {}
			for key in config[section]:
				WiabMod.items[section][key] = config[section][key]
		
		config = ConfigParser()
		config.read(os.path.join('mods', 'default', 'starting_units.ini'))
		for key in config["default"]:
			WiabMod.starting_units.append(config["default"][key])
		
		config = ConfigParser()
		config.read(os.path.join('mods', 'default', 'terrains.ini'))
		for section in config.sections():
			WiabMod.terrains[section] = {}
			for key in config[section]:
				WiabMod.terrains[section][key] = config[section][key]
			
			if is_client:
				WiabMod.terrain_pixmaps[section] = QPixmap(os.path.join('mods', 'default', 'terrains', section + '.png'))
		
		config = ConfigParser()
		config.read(os.path.join('mods', 'default', 'units.ini'))
		for section in config.sections():
			WiabMod.units[section] = {}
			for key in config[section]:
				WiabMod.units[section][key] = config[section][key]
			
			
			if is_client:
				WiabMod.unit_pixmaps[section] = QPixmap(os.path.join('mods', 'default', 'units', section + '.png'))
		
		print(WiabMod.buildings)
		print(WiabMod.items)
		print(WiabMod.starting_units)
		print(WiabMod.terrains)
		print("unit_pixmaps:", WiabMod.unit_pixmaps)
		print(WiabMod.units)
