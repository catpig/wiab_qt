# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

# This class is for the game model's view of a player. It does not deal with connection related things.

#python std lib

#pyqt

#other libs

#own modules

class Player(object):
	def __init__(self, player_id, name):
		self.player_id = player_id
		self.name = name
		self.units = []
		self.diplomatic_states = {} #player_id:state (where state is "war" or "peace")
