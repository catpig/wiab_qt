# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib

#pyqt

#other libs
#import typing

#own modules


class Thing(object):
	next_thing_id = 0
	
	def __init__(self, owner, location, thing_id):
		self.owner = owner
		self.location = location
		if thing_id:
			self.thing_id = thing_id
			if thing_id <= Thing.next_thing_id:
				Thing.next_thing_id = thing_id + 1
		else:
			self.thing_id = Thing.next_thing_id
		Thing.next_thing_id += 1
	
	def end_turn(self):
		pass
