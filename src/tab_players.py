#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib

#pyqt
from PyQt5.QtCore import QObject #pyqtSignal, 
from PyQt5.QtWidgets import QGridLayout, QLabel, QLayout, QScrollArea, QWidget

#other libs

#own modules

class TabPlayers(QObject):
	def __init__(self, parent): #TODO find what parent did exactly and then apply it accordingly everywhere
		super(TabPlayers, self).__init__()
		self.active_player_id = 0
		self.parent = parent
		
		self.scroller = QScrollArea()
		self.widget = QWidget()
		self.main_grid = QGridLayout()
		self.main_grid.setSizeConstraint(QLayout.SetMinAndMaxSize)
		self.widget.setLayout(self.main_grid)
		self.scroller.setWidget(self.widget)
	
	def draw(self):
		while(True): #TODO move this part up to setting row_counter etc into GridScrollTab
			item = self.main_grid.takeAt(0)
			if not item:
				break
			item.widget().deleteLater()
		
		grid_x = 0
		grid_y = 0
		
		for header_item in ("ID", "Name", "Diplomatic State", "Change State"):
			self.main_grid.addWidget(QLabel(header_item), grid_y, grid_x, 1, 1)
			grid_x += 1
		grid_x = 0
		grid_y += 1
		
		for player in self.parent.game.players.values():
			if player.player_id == 0: #"unowned" player
				continue
			
			if self.parent.active_player_id == player.player_id:
				id_str = "%d (you)" % player.player_id
			else:
				id_str = str(player.player_id)
			self.main_grid.addWidget(QLabel(id_str), grid_y, grid_x, 1, 1)
			grid_x += 1
			
			self.main_grid.addWidget(QLabel(player.name), grid_y, grid_x, 1, 1)
			grid_x += 1
			
			if self.parent.active_player_id == player.player_id:
				diplo_str = "n/a (you)"
			else:
				diplo_str = self.parent.game.players[self.parent.active_player_id].diplomatic_states[player.player_id]
			self.main_grid.addWidget(QLabel(diplo_str), grid_y, grid_x, 1, 1)
			grid_x += 1
			
			self.main_grid.addWidget(QLabel("placeholder"), grid_y, grid_x, 1, 1)
			
			grid_x = 0
			grid_y += 1
